package com.example.correabrayan_examen3.controlador;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.correabrayan_examen3.modelo.Persona;

import java.util.ArrayList;
import java.util.List;

public class controladorHelper extends SQLiteOpenHelper {

    public controladorHelper(Context context) {
        //String name, SQLiteDatabase.CursorFactory factory, int version
        super(context, "BrayanBD", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Persona (id INTEGER PRIMARY KEY AUTOINCREMENT,cedula INTEGER UNIQUE," + "apellidosnombres TEXT, " +
                "recinto TEXT, junta TEXT, direccion TEXT, provincia TEXT, canton TEXT );");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //  db.execSQL("UPDATE");
    }

    private ContentValues values = new ContentValues();

    public void insertar (Persona Persona){

        values.put("cedula", Persona.getCedula());
        values.put("apellidosnombres", Persona.getApellidosnombres());
        values.put("recinto", Persona.getRecinto());
        values.put("junta", Persona.getJunta());
        values.put("direccion", Persona.getDireccion());
        values.put("provincia", Persona.getProvincia());
        values.put("canton", Persona.getCanton());

        this.getWritableDatabase().insert("Persona",null,values);
    }




    public List<Persona> getAllHelper(){
        List <Persona> lista = new ArrayList<Persona>();
        Cursor cursor= this.getWritableDatabase().rawQuery("select *from Persona", null);
        if(cursor.moveToFirst()){
            do {
                Persona Ver = new Persona();
                Ver.setCedula(cursor.getString(cursor.getColumnIndex("cedula")));
                Ver.setApellidosnombres(cursor.getString(cursor.getColumnIndex("apellidosnombres")));
                Ver.setRecinto(cursor.getString(cursor.getColumnIndex("recinto")));
                Ver.setJunta(cursor.getString(cursor.getColumnIndex("junta")));
                Ver.setDireccion(cursor.getString(cursor.getColumnIndex("direccion")));
                Ver.setProvincia(cursor.getString(cursor.getColumnIndex("provincia")));
                Ver.setCanton(cursor.getString(cursor.getColumnIndex("canton")));
                lista.add(Ver);

            }
            while(cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }
    public List<Persona> getByCode(String cedula){
        List <Persona> lista = new ArrayList<Persona>();
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM Persona where cedula = '" + cedula + "'",null);
        if (cursor.moveToFirst()){
            do{
                Persona hel = new Persona();
                hel.setCedula(cursor.getString(cursor.getColumnIndex("cedula")));
                hel.setApellidosnombres(cursor.getString(cursor.getColumnIndex("apellidosnombres")));
                hel.setRecinto(cursor.getString(cursor.getColumnIndex("recinto")));
                hel.setJunta(cursor.getString(cursor.getColumnIndex("junta")));
                hel.setDireccion(cursor.getString(cursor.getColumnIndex("direccion")));
                hel.setProvincia(cursor.getString(cursor.getColumnIndex("provincia")));
                hel.setCanton(cursor.getString(cursor.getColumnIndex("canton")));

                lista.add(hel);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }
}

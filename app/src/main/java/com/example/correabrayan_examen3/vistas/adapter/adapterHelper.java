package com.example.correabrayan_examen3.vistas.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.correabrayan_examen3.R;
import com.example.correabrayan_examen3.modelo.Persona;

import java.util.List;

public class adapterHelper extends RecyclerView.Adapter<adapterHelper.ViewHolder> implements View.OnClickListener{

    List<Persona> lista;
    private View.OnClickListener listener;
    public adapterHelper(List<Persona> lista) {
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item_helper, null);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int posicion) {
        //se fijan los datos en cada fila a traves de l holder view
        viewHolder.cedula.setText(lista.get(posicion).getCedula());
        viewHolder.apellidosnombres.setText(lista.get(posicion).getApellidosnombres());
        viewHolder.recinto.setText(lista.get(posicion).getRecinto());
        viewHolder.junta.setText(lista.get(posicion).getJunta());
        viewHolder.direccion.setText(lista.get(posicion).getDireccion());
        viewHolder.provincia.setText(lista.get(posicion).getProvincia());
        viewHolder.canton.setText(lista.get(posicion).getCanton());
        }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;

    }
    @Override
    public void onClick(View v) {
        if (listener != null ){
            listener.onClick(v);
        }
    }



    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView cedula;
        TextView apellidosnombres;
        TextView recinto;
        TextView junta;
        TextView direccion;
        TextView provincia;
        TextView canton;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            cedula = itemView.findViewById(R.id.lblCedulaCard);
            apellidosnombres = itemView.findViewById(R.id.lblApellidosNombresCard);
            recinto = itemView.findViewById(R.id.lblRecintoCard);
            junta = itemView.findViewById(R.id.lblJuntaCard);
            direccion = itemView.findViewById(R.id.lblDireccionCard);
            provincia = itemView.findViewById(R.id.lblProvinciaCard);
            canton = itemView.findViewById(R.id.lblCantonCard);
        }
    }
}

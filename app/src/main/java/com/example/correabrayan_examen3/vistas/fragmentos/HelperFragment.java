package com.example.correabrayan_examen3.vistas.fragmentos;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.correabrayan_examen3.R;
import com.example.correabrayan_examen3.controlador.controladorHelper;
import com.example.correabrayan_examen3.modelo.Persona;
import com.example.correabrayan_examen3.vistas.adapter.adapterHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HelperFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HelperFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelperFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View vista;
    EditText cajaCedula, cajaApellidosNombres, cajaRecinto, cajaJunta, cajaDireccion, cajaProvincia, cajaCanton;
    Button botonAgregar, botonBuscarCedula;
    controladorHelper controladorhelper;
    RecyclerView recyclerViewhelper;
    adapterHelper adapter;
    private List<Persona> list;

    private OnFragmentInteractionListener mListener;

    public HelperFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HelperFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelperFragment newInstance(String param1, String param2) {
        HelperFragment fragment = new HelperFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void tomarControl(){
        cajaCedula = vista.findViewById(R.id.txtCedulaHelper);
        cajaApellidosNombres = vista.findViewById(R.id.txtApellidosNombresHelper);
        cajaRecinto = vista.findViewById(R.id.txtRecintoHelper);
        cajaJunta = vista.findViewById(R.id.txtJuntaHelper);
        cajaDireccion = vista.findViewById(R.id.txtDireccionHelper);
        cajaProvincia = vista.findViewById(R.id.txtProvinciaHelper);
        cajaCanton = vista.findViewById(R.id.txtCantonHelper);
        botonAgregar= vista.findViewById(R.id.btnAgregarHelper);
        botonBuscarCedula = vista.findViewById(R.id.btnBuscarCedulaHelper);


        recyclerViewhelper = (RecyclerView) vista.findViewById(R.id.recyclerHelper);

        botonAgregar.setOnClickListener(this);
        botonBuscarCedula.setOnClickListener(this);

    }


    public void cargarLista(List<Persona> listaPersona){
        list = new ArrayList<Persona>();
        list = listaPersona;
        recyclerViewhelper.setLayoutManager(new LinearLayoutManager(vista.getContext()));
        controladorhelper = new controladorHelper(vista.getContext());
        adapter = new adapterHelper(controladorhelper.getAllHelper());


        adapter = new adapterHelper(list);
        adapter.setOnClickListener(new View.OnClickListener(){



            @Override
            public void onClick(View v)
            {
                Persona help = list.get(recyclerViewhelper.getChildAdapterPosition(v));

                cajaCedula.setText(help.getCedula());
                cajaApellidosNombres.setText(help.getApellidosnombres());
                cajaRecinto.setText(help.getRecinto());
                cajaJunta.setText(help.getJunta());
                cajaDireccion.setText(help.getDireccion());
                cajaProvincia.setText(help.getProvincia());
                cajaCanton.setText(help.getCanton());

            }
        });
        recyclerViewhelper.setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        vista = inflater.inflate(R.layout.fragment_helper, container, false);
        tomarControl();
        return vista;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        controladorhelper = new controladorHelper(vista.getContext());
        adapter = new adapterHelper(controladorhelper.getAllHelper());

        switch (v.getId()){

            case R.id.btnAgregarHelper:
                Persona hel = new Persona();
                hel.setCedula(cajaCedula.getText().toString());
                hel.setApellidosnombres(cajaApellidosNombres.getText().toString());
                hel.setDireccion(cajaDireccion.getText().toString());
                hel.setCanton(cajaCanton.getText().toString());
                hel.setJunta(cajaJunta.getText().toString());
                hel.setRecinto(cajaRecinto.getText().toString());
                hel.setProvincia(cajaProvincia.getText().toString());
                controladorhelper.insertar(hel);

                cargarLista(controladorhelper.getAllHelper());
                Toast.makeText(getActivity(), "Agregado Correctamente", Toast.LENGTH_SHORT).show();
                break;



            case R.id.btnBuscarCedulaHelper:
                cargarLista(controladorhelper.getByCode(cajaCedula.getText().toString()));
                break;


        }
    }




    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

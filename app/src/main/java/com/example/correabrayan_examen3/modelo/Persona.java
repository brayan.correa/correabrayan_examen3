package com.example.correabrayan_examen3.modelo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;


public class Persona {
    private String cedula;
    private String apellidosnombres;
    private String recinto;
    private String junta;
    private String direccion;
    private String provincia;
    private String canton;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }


    public String getApellidosnombres() {
        return apellidosnombres;
    }

    public void setApellidosnombres(String apellidosnombres) {
        this.apellidosnombres = apellidosnombres;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRecinto() {
        return recinto;
    }

    public void setRecinto(String recinto) {
        this.recinto = recinto;
    }

    public String getJunta() {
        return junta;
    }

    public void setJunta(String junta) {
        this.junta = junta;
    }



    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    @Override
    public String toString() {
        return cedula+ " , "+apellidosnombres+" , " + recinto+" , "+junta+" , "+direccion+" , "+provincia+" , "+canton;
    }


}
